# Guertel noise
The flat where I used to live faced the Guertel, here in Vienna. The Guertel ("belt") it is a four-lanes-per-direction street that cuts in half the city, splitting the west-central districts from Penzing, Ottakring, Währing. 

Being concerned not only with the air pollution, but also with the noise pollution, I decided to record it with a microphone. Here I created a Python notebook which extracts some features from these audio tracks, trying to characterize 
